<?php

// One Way to declare
//namespace Src\Bitm\SEIP129900\BookTitle;

// Second Way to declare


// Third Way to declare
namespace App\Bitm\SEIP129900\BookTitle;
class BookTitle
{
    public $id ="";
    public $title="";
    public $created="";
    public $modified="";
    public $created_by="";
    public $modified_by="";

    public function index(){
        echo"I'm listing data.";
    }
    public function create(){
        echo"I'm create form.";
    }
    public function store(){
        echo"I'm sorting data.";
    }
    public function edit(){
        echo"I'm editing data.";
    }
    public function update(){
        echo"I'm updating data.";
    }
    public function delete(){
        echo"I delete data.";
    }
    public function view(){
        echo"I'm view data.";
    }
}


?>