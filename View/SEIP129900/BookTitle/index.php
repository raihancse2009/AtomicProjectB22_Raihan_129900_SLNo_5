<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 6/5/2016
 * Time: 7:05 PM
 */

// One Way to declare
/*include_once ('../../../Src/Bitm/SEIP129900/BookTitle/BookTitle.php');
use Src\Bitm\SEIP129900\BookTitle\BookTitle;*/

// Second Way to declare
/*function __autoload($ClassName){
    $fileName = "../../../".$ClassName.".php";
    include_once($fileName);
}*/
/*$classname = "../../../Src/Bitm/SEIP129900/BookTitle/BookTitle";
function __autoload($classname) {
    $filename = "./". $classname .".php";
    include_once($filename);
}
use Src\Bitm\SEIP129900\BookTitle\BookTitle;*/

// Third Way to declare
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP129900\BookTitle\BookTitle;

$Book = new BookTitle();
$Book->index();

?>